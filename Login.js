describe("Login", () => {
  it("tests Login", () => {
    cy.viewport(1047, 718);
    cy.visit("http://localhost:5173/#/login");
    cy.get("#username").click();
    cy.get("#username").type("janedoe");
    cy.get("form > div").click();
    cy.get("#password").click();
    cy.get("#password").type("password");
    cy.get("button").click();
    cy.get("[data-testid='Brightness4Icon']").click();
    cy.get("[data-testid='Brightness7Icon']").click();
  });
});
