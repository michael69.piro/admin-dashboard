
import { Layout } from 'react-admin';
import { ReactQueryDevtools } from 'react-query/devtools';

export const CustomLayout = props => (
    <>
        <Layout {...props} />
        <ReactQueryDevtools initialIsOpen={false} />
    </>
);
