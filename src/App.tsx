
import { Admin, Resource, ShowGuesser, defaultTheme } from 'react-admin';
import { dataProvider } from './dataProvider';
import { authProvider } from './authProvider';
import { BookList, BookEdit, BookCreate } from './views/books/books';
import { AuthorList, AuthorEdit, AuthorCreate } from './views/authors/authors';
import { UserList, UserEdit, UserCreate } from './views/users/users';
import { LoginPage } from './views/login/LoginPage';
import { CustomLayout } from './layout';

const themeBase = {
	...defaultTheme,
	sidebar: {
		width: 140
	}
}

const lightTheme = {
	...themeBase,
};

const darkTheme = { ...themeBase, palette: { mode: 'dark' } };

export const App = () => (
	<Admin
		loginPage={LoginPage}
		layout={CustomLayout}
		theme={lightTheme}
		darkTheme={darkTheme}
		dataProvider={dataProvider}
		authProvider={authProvider}
	>
		{/* <Resource name="admin" list={ListGuesser} edit={EditGuesser}   /> */}
		<Resource name="users" list={UserList} edit={UserEdit} create={UserCreate} />
		<Resource name="books" list={BookList} edit={BookEdit} create={BookCreate} />
		<Resource name="authors" list={AuthorList} edit={AuthorEdit} create={AuthorCreate} recordRepresentation="name" />
	</Admin>
);

