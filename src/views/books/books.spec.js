import React from "react";
import { AdminContext, memoryStore, testDataProvider } from "react-admin";
import { render, screen } from "@testing-library/react";
import { BookList } from "./books";
import "@testing-library/jest-dom";

let books = [
  {
    id: 1,
    title: "El gran Gatsby",
    authorId: 1,
    publicationYear: 1925,
  },
  {
    id: 2,
    title: "1984",
    authorId: 2,
    publicationYear: 1949,
  },
  {
    id: 3,
    title: "Cien años de soledad",
    authorId: 3,
    publicationYear: 1967,
  },
  {
    id: 4,
    title: "Matar a un ruiseñor",
    authorId: 4,
    publicationYear: 1960,
  },
  {
    id: 5,
    title: "Don Quijote de la Mancha",
    authorId: 5,
    publicationYear: 1605,
  },
  {
    authorId: 6,
    title: "Never ends",
    publicationYear: 2020,
    id: 11,
  },
  {
    authorId: 6,
    title: "Another brick in the wall",
    publicationYear: 2010,
    id: 12,
  },
  {
    authorId: 5,
    title: "libro de prueba",
    publicationYear: 2020,
    id: 13,
  },
  {
    id: 14,
    title: "To Kill a Mockingbird",
    authorId: 4,
    publicationYear: 1960,
  },
  {
    id: 15,
    title: "The Catcher in the Rye",
    authorId: 7,
    publicationYear: 1951,
  },
  {
    id: 16,
    title: "One Hundred Years of Solitude",
    authorId: 3,
    publicationYear: 1967,
  },
  {
    id: 17,
    title: "Animal Farm",
    authorId: 2,
    publicationYear: 1945,
  },
  {
    id: 18,
    title: "Fahrenheit 451",
    authorId: 8,
    publicationYear: 1953,
  },
  {
    id: 19,
    title: "Pride and Prejudice",
    authorId: 9,
    publicationYear: 1813,
  },
  {
    id: 20,
    title: "Jane Eyre",
    authorId: 10,
    publicationYear: 1847,
  },
  {
    id: 21,
    title: "The Great Gatsby",
    authorId: 1,
    publicationYear: 1925,
  },
  {
    id: 22,
    title: "Brave New World",
    authorId: 11,
    publicationYear: 1932,
  },
  {
    id: 23,
    title: "1984",
    authorId: 2,
    publicationYear: 1949,
  },
  {
    id: 24,
    title: "To the Lighthouse",
    authorId: 12,
    publicationYear: 1927,
  },
  {
    id: 25,
    title: "The Lord of the Rings",
    authorId: 13,
    publicationYear: 1954,
  },
  {
    id: 26,
    title: "Crime and Punishment",
    authorId: 14,
    publicationYear: 1866,
  },
  {
    id: 27,
    title: "The Odyssey",
    authorId: 15,
    publicationYear: -800,
  },
  {
    id: 28,
    title: "War and Peace",
    authorId: 16,
    publicationYear: 1869,
  },
  {
    id: 29,
    title: "The Picture of Dorian Gray",
    authorId: 17,
    publicationYear: 1890,
  },
  {
    id: 30,
    title: "Alice's Adventures in Wonderland",
    authorId: 18,
    publicationYear: 1865,
  },
];

test("Should render <BookList /> with empty values.", async () => {
  let dataProvider = testDataProvider({
    getList: function (resource, params) {
      return Promise.resolve({ data: books, total: 30 });
    },
  });

  render(
    <AdminContext dataProvider={dataProvider}>
      <BookList resource="books" />
    </AdminContext>
  );

  expect(await screen.findByText("El gran Gatsby")).toBeInTheDocument();
});

test.skip("CRUD flow for Books", async () => {
  render(
    <AdminContext store={memoryStore()}>
      <BookList />
    </AdminContext>
  );

  screen.debug();
  const items = await screen.findAllByText(/Item #[0-9]: /);
  expect(items).toHaveLength(10);
});
