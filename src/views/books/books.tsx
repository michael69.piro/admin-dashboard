import React from 'react';
import { Datagrid, List, NumberField, ReferenceField, TextField, EditButton, Edit, NumberInput, ReferenceInput, SimpleForm, TextInput, Create } from 'react-admin';

export const BookCreate = () => (
    <Create>
        <SimpleForm>
            <ReferenceInput source="authorId" reference="authors" />
            <TextInput source="title" />
            <NumberInput source="publicationYear" />
        </SimpleForm>
    </Create>
);


export const BookEdit = () => (
    <Edit>
        <SimpleForm>
            <TextInput source="id" disabled />
            <TextInput source="title" />
            <ReferenceInput source="authorId" reference="authors" />
            <NumberInput source="publicationYear" />
        </SimpleForm>
    </Edit>
);

export const BookList = () => (
    <List resource="books">
        <Datagrid rowClick="edit">
            <TextField source="id" />
            <TextField source="title" />
            <ReferenceField source="authorId" reference="authors" />
            <NumberField source="publicationYear" />
            <EditButton />
        </Datagrid>
    </List>
);