import React from "react";
import { useMediaQuery } from "@mui/material";
import { Datagrid, SimpleList, List, NumberField, TextField, Edit, NumberInput, SimpleForm, TextInput, Create, ReferenceInput, useRecordContext, Button, Link, ResourceContextProvider } from 'react-admin';

export const AuthorCreate = () => (
    <Create>
        <SimpleForm>
            <ReferenceInput source="authorId" reference="authors" />
            <TextInput source="name" />
            <NumberInput source="birthYear" />
            <TextInput source="nationality" />
        </SimpleForm>
    </Create>
);

export const AuthorEdit = () => (
    <Edit>
        <SimpleForm>
            <TextInput source="id" disabled />
            <TextInput source="name" />
            <NumberInput source="birthYear" />
            <TextInput source="nationality" />
        </SimpleForm>
    </Edit>
);

const BooksButton = () => {
    const record = useRecordContext();
    console.log('record', record);

    return (
        <Button
            component={Link}
            to={`/authors/${record.id}/books`}
            color="primary"
        >
            Books
        </Button>
    );
};

export const AuthorList = () => {
    const isSmall = useMediaQuery((theme) => theme.breakpoints.down("sm"))

    return (
        <List resource="authors">
            {
                isSmall ? (
                    <SimpleList
                        primaryText={(record) => record.name}
                        secondaryText={(record) => record.birthYear}
                        tertiaryText={(record) => record.nationality}
                    />
                ) : (
                    <Datagrid rowClick="edit">
                        <TextField source="id" />
                        <TextField source="name" />
                        <NumberField source="birthYear" />
                        <TextField source="nationality" />
                        <BooksButton />
                    </Datagrid>
                )
            }
        </List>
    )
}; 