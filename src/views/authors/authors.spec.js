import React from "react";
import { AdminContext } from "react-admin";
import { render, screen } from "@testing-library/react";
import { AuthorList } from "./authors";

test("<AuthorList>", async () => {
  render(
    <AdminContext
      dataProvider={{
        getOne: () => Promise.resolve({ data: { id: 1, name: "foo" } }),
      }}
    >
      <AuthorList resource="authors" />
    </AdminContext>
  );
  //const items = await screen.findAllByText(/Item #[0-9]: /);
  //expect(items).toHaveLength(10);

  screen.debug();
});
