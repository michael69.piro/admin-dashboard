import { useMediaQuery } from "@mui/material";
import { List, SimpleList, Datagrid, TextField, Edit, SimpleForm, TextInput, Create } from "react-admin";
import { useParams } from 'react-router-dom';

export const UserCreate = () => (
    <Create>
        <SimpleForm>
            <TextInput source="id" />
            <TextInput source="username" />
            <TextInput source="password" />
            <TextInput source="fullName" />
            <TextInput source="avatar" />
        </SimpleForm>
    </Create>
);

export const UserEdit = () => (
    <Edit>
        <SimpleForm>
            <TextInput source="id" disabled/>
            <TextInput source="username" />
            <TextInput source="password" />
            <TextInput source="fullName" />
            <TextInput source="avatar" />
        </SimpleForm>
    </Edit>
);

export const UserList = () => {
    const isSmall = useMediaQuery((theme) => theme.breakpoints.down("sm"));
    return (
      <List>
        {isSmall ? (
          <SimpleList
          primaryText={(record) => record.fullName}
          secondaryText={(record) => record.username}
          tertiaryText={(record) => record.password}
          />
          ) : (
            <Datagrid rowClick="edit">
            <TextField source="id" />
            <TextField source="username" />
            <TextField source="password" />
            <TextField source="fullName" />
          </Datagrid>
        )}
      </List>
    );
  };

