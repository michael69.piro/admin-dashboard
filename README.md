# admin-dashboard

## Credentials

To start working, run cd admin-dashboard.
Start the app in development mode by running yarn dev.
You can sign in to the application with the following usernames and password:

- janedoe / password
- johndoe / password

## Installation

Install the application dependencies by running:

```sh
npm install
```

Run server:

```
npm run server
```

## Development

Start the application in development mode by running:

```sh
npm run dev
```

## Production

Build the application in production mode by running:

```sh
yarn build
```

## Run unit test

```
npm run test
```

See coverage report in:

```
admin-dashboard/coverage/lcov-report/index.html
```

## Docker

Open a console, and build an image of this project.

```
docker build -t admin-dashboard:DEV .
```

Open a console, run the following command to run the container.

```
docker run --rm -it -p 80:80/tcp admin-dashboard:DEV
```

Open a browser, go to:

```
http://localhost/#/login
```

You can sign in to the application with the following usernames and password:

- janedoe / password
- johndoe / password

## Authentication

The included auth provider should only be used for development and test purposes.
You'll find a `user object in db.json file` that includes the users you can use.

You can sign in to the application with the following usernames and password:

- janedoe / password
- johndoe / password

## Improvements

    - Added dockerfile
        -- Server and a production version of the UI are up.
    - Added dark theme.
    - Added users in case you need to manage users.
    - Added author name  (id show as default) in the books table.
    - Added library for unit test.
    - Some views has media querys from responsive sizes.

## TO DO's / Dev notes

    - add before each for login action.
    - add redirect to user when user is created.
    - add userType or isAdmin to give access to different views.
    - provider is not working as expected in the test. Need to see how the data is pass in the context.
    - fix unit test
        -- authors.spec is not returning data from the context.
