# stage1 - build react app first 
FROM node:16.14.0 as build
WORKDIR /app
COPY . .
COPY .env /app/
RUN npm install
RUN npm run build
CMD ["npm", "run", "server"]

# stage 2 - build the final image and copy the react build files
FROM nginx:1.23.4-alpine-slim
COPY --from=build /app/dist /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
RUN rm /etc/nginx/nginx.conf
COPY nginx/nginx.conf /etc/nginx/nginx.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]