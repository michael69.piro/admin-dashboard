/* 
  TODO: 
      - add before each for login action.
      - add redirect to user when user is created.
      - add userType or isAdmin to give access to different views.
*/

describe("userCrud", () => {
    it("tests userCrud", () => {
        cy.viewport(1047, 718);

        // Login 
        cy.visit("http://localhost:5173/#/login");
        cy.get("#username").click();
        cy.get("#username").type("janedoe");
        cy.get("form > div").click();
        cy.get("#password").click();
        cy.get("#password").type("password");
        cy.get("button").click();

        // Add new user
        cy.visit("http://localhost:5173/#/users");
        cy.get("#main-content a").click();
        cy.get("#id").click();
        cy.get("#id").type("56");
        cy.get("#username").click();
        cy.get("#username").type("jose.perez");
        cy.get("#password").click();
        cy.get("#password").type("123qwe");
        cy.get("#fullName").click();
        cy.get("#fullName").type("Jose Perez");
        cy.get("#avatar").click();
        cy.get("#avatar").type("none");
        cy.get("main button").click();

        // back to user menu
        cy.get("a.RaMenuItemLink-active").click();

        // delete user
        cy.get("tr:nth-of-type(4) > td.column-username").click();
        cy.get("main button.MuiButton-text").click();

        // edit user password
        cy.get("tr:nth-of-type(3) > td.column-username > span").click();
        cy.get("#password").click();
        cy.get("#password").type("1");
        cy.get("#root > div > div > div > div button.MuiButton-contained").click();
    });
});
