describe("booksCrud", () => {
    it("tests booksCrud", () => {
        cy.viewport(1047, 718);

        // Login 
        cy.visit("http://localhost:5173/#/login");
        cy.get("#username").click();
        cy.get("#username").type("janedoe");
        cy.get("form > div").click();
        cy.get("#password").click();
        cy.get("#password").type("password");
        cy.get("button").click();

        // Get all books
        cy.visit("http://localhost:5173/#/books");

        // Create Book
        cy.get("a.RaMenuItemLink-active").click();
        cy.get("#main-content a").click();
        cy.get("#authorId").click();
        cy.get("#authorId-option-1").click();
        cy.get("#title").click();
        cy.get("#title").type("libro de prueba");
        cy.get("#publicationYear").click();
        cy.get("#publicationYear").type("2020");
        cy.get("main div.MuiToolbar-root button").click();
        cy.get("a.RaMenuItemLink-active > div").click();

        // Edit Book
        cy.get("tr:nth-of-type(6) > td.column-title").click();
        cy.get("#title").click();
        cy.get("#title").type("edito libro de prueba");
        cy.get("#root > div > div > div > div button.MuiButton-contained").click();
        cy.get("tr:nth-of-type(6) > td.column-title").click();
        cy.get("[data-testid='DeleteIcon']").click();
        cy.get("#main-content").click();
    });
});
